package com.tuenti.tuentitestsantiago.domain;

import com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse;
import com.tuenti.tuentitestsantiago.domain.model.Name;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.repository.ReadOnlyDataSource;
import com.tuenti.tuentitestsantiago.view.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * A network sourcre which returns 3 users, 2 are duplicated
 * Created by seven on 22/04/2017.
 */

public class MockNetworkSource implements ReadOnlyDataSource{
    
    private List<User> users;
    
    public MockNetworkSource(){
        users = new ArrayList<>();
        User user1 = new User();
        Name name = new Name();
        name.setFirst("Fake 1");
        name.setLast("FakeLast");
        user1.setDob("1988-01-05");
        user1.setRegistered("2021-12-12");
        user1.setName(name);
        user1.setEmail("fake@fake.com");
        User user2 = new User();
        Name name2 = new Name();
        name2.setFirst("Fake 2");
        name2.setLast("FakeLast2");
        user1.setDob("1989-01-05");
        user1.setRegistered("2021-12-12");
        user2.setName(name2);
        user2.setEmail("fake@fake.com");
        users.add(user2);
        User user3 = new User();
        Name name3 = new Name();
        name3.setFirst("Fake 1");
        name3.setLast("FakeLast");
        user3.setDob("1988-01-05");
        user3.setRegistered("2021-12-12");
        user3.setName(name3);
        user3.setEmail("fake@fake.com");
        users.add(user3);
    }
    

    @Override
    public DataResponse<List<User>> getUsers() {
        return new DataResponse<>(users);
    }

    @Override
    public DataResponse<User> getUserById(String id) {
        User user = users.stream().filter(it -> it.getId() == Integer.valueOf(id)).findFirst().orElse(null);
        if (user == null) {
            return new DataResponse<User>(Constants.Errors.NO_USER);
        }
        return new DataResponse<>(user);
    }

    @Override
    public boolean hasUserDatasetLoaded() {
        return true;
    }
    
}
