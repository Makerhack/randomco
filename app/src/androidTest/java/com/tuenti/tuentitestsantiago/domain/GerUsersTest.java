package com.tuenti.tuentitestsantiago.domain;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.busresponses.UsersResponse;
import com.tuenti.tuentitestsantiago.domain.usecases.GetUsersUseCase;
import com.tuenti.tuentitestsantiago.repository.UserRepository;
import com.tuenti.tuentitestsantiago.repository.local.AndroidDatabaseSource;
import com.tuenti.tuentitestsantiago.repository.local.DataBaseHelper;
import com.tuenti.tuentitestsantiago.view.base.common.MainThreadBus;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertTrue;

/**
 * Class to unitest users
 * Created by seven on 22/04/2017.
 */
@RunWith(AndroidJUnit4.class)
public class GerUsersTest {

    private Bus bus = new MainThreadBus();

    @Test
    public void TestGerUsers() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        DataBaseHelper helper = new DataBaseHelper(appContext, "test", null, 1);
        UserRepository userRepository = new UserRepository(new MockNetworkSource(), new AndroidDatabaseSource(helper));
        GetUsersUseCase usersUseCase = new GetUsersUseCase(bus, userRepository);
        usersUseCase.execute();
        UsersResponse all = userRepository.getAll();
        assertTrue(all != null && !all.hasError());
        Log.d(getClass().getSimpleName(),"Size is: " +all.getResponse().size());
        assertTrue(all.getResponse().size() == 2);
    }

}
