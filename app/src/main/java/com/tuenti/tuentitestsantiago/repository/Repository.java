package com.tuenti.tuentitestsantiago.repository;

import com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse;

import java.util.List;

/**
 * Inteface representing a data repository
 * Created by seven on 22/04/2017.
 */

public interface Repository<T, K> {

    DataResponse<T> getByKey(K key);

    DataResponse<List<T>> getAll();

    boolean deleteByKey(K key);

    void deleteAll();

    DataResponse<List<T>> getPaginated();

    DataResponse<List<T>> getMore();

    boolean save(T data);

    void saveBulk(List<T> data);
}
