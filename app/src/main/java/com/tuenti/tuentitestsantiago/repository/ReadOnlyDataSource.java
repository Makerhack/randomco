package com.tuenti.tuentitestsantiago.repository;

import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse;

import java.util.List;

public interface ReadOnlyDataSource {

    DataResponse<List<User>> getUsers();

    DataResponse<User> getUserById(String id);

    boolean hasUserDatasetLoaded();

}
