package com.tuenti.tuentitestsantiago.repository;


import com.tuenti.tuentitestsantiago.repository.ReadOnlyDataSource;
import com.tuenti.tuentitestsantiago.repository.DataStorage;


/**
 * Created by seven on 22/04/2017.
 */

public interface WritableDataSource extends DataStorage, ReadOnlyDataSource {
    boolean isCacheInvalidated();
}
