package com.tuenti.tuentitestsantiago.repository.network.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tuenti.tuentitestsantiago.domain.model.User;

import java.util.List;

public class JsonModel {

    @SerializedName("results")
    @Expose
    private List<User> results = null;

    public List<User> getResults() {
        return results;
    }

}