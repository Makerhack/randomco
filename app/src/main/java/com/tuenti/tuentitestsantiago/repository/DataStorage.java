package com.tuenti.tuentitestsantiago.repository;

import com.tuenti.tuentitestsantiago.domain.model.User;

import java.util.List;

public interface DataStorage {

    void storeUsers(List<User> transactions);
    int deleteUser(String userId);
    void clealUsers();
    int storeUser(User data);
}
