package com.tuenti.tuentitestsantiago.repository.network;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.google.common.reflect.TypeToken;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse;
import com.tuenti.tuentitestsantiago.repository.ReadOnlyDataSource;
import com.tuenti.tuentitestsantiago.repository.network.base.NetworkConnection;
import com.tuenti.tuentitestsantiago.repository.network.base.GsonRequest;
import com.tuenti.tuentitestsantiago.repository.network.base.JsonModel;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Wrapper to communicate with the server
 * Created by Santi
 */
public class AndroidNetworkDataSource extends NetworkConnection implements ReadOnlyDataSource {

    public AndroidNetworkDataSource(RequestQueue requestQueue) {
        super(requestQueue);
    }

    @Override
    public DataResponse<List<User>> getUsers() {
        Type type = new TypeToken<JsonModel>() {

        }.getType();
        GsonRequest.Builder<JsonModel> builder = new GsonRequest.Builder<>(type,
                Request.Method.GET, "", "?results=40");
        DataResponse<JsonModel> dataResponse = performNetworkRequest(builder);
        if (!dataResponse.hasError()) {
            JsonModel response = dataResponse.getResponse();
            return new DataResponse<>(response.getResults());
        }
        return new DataResponse<List<User>>(dataResponse.getError());
    }

    @Override
    public DataResponse<User> getUserById(String id) {
        return null;
    }

    @Override
    public boolean hasUserDatasetLoaded() {
        return false;
    }


    /*@Override
    public EstimatesResponse getStops(StopsRequest request) {
        Type type = new TypeToken<List<Estimate>>() {

        }.getType();
        String jsonRequest = gson.toJson(request);
        GsonRequest.Builder<List<Estimate>> builder = new GsonRequest.Builder<>(type,
                Request.Method.POST, jsonRequest, "api/v2/estimate");
        DataResponse<List<Estimate>> response = performNetworkRequest(builder);
        List<Estimate> estimates = response.getResponse();
        EstimatesResponse finalResponse = new EstimatesResponse();
        if (estimates != null) {
            finalResponse.setResponse(estimates);
        } else {
            finalResponse.setError(response.getError());
        }
        return finalResponse;
    }*/


}
