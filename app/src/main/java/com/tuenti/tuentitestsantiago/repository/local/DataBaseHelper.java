package com.tuenti.tuentitestsantiago.repository.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.tuenti.tuentitestsantiago.domain.model.User;

import java.sql.SQLException;


public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private Context context;
    private Dao<User, Integer> usersDao;

    public DataBaseHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
        this.context = context;
    }


    public Context getContext() {
        return context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, User.class);
        } catch (SQLException e) {
            Log.e(DataBaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    Dao<User, Integer> getUsersDao() {
        if (usersDao == null) {
            try {
                usersDao = getDao(User.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return usersDao;
    }
}