package com.tuenti.tuentitestsantiago.view.base;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.repository.UserRepository;
import com.tuenti.tuentitestsantiago.repository.local.AndroidDatabaseSource;
import com.tuenti.tuentitestsantiago.repository.local.DataBaseHelper;
import com.tuenti.tuentitestsantiago.repository.network.AndroidNetworkDataSource;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.executor.AndroidUseCaseCaller;
import com.tuenti.tuentitestsantiago.view.executor.ExecutorIntentService;
import com.tuenti.tuentitestsantiago.view.base.common.MainThreadBus;
import com.tuenti.tuentitestsantiago.domain.UseCaseCaller;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true,
        injects = {
                BaseApplication.class, ExecutorIntentService.class
        })
class AppModule {
    private final Application application;

    AppModule(Application application) {
        this.application = application;
    }

    @Provides
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public Bus provideBus() {
        return new MainThreadBus();
    }

    @Provides
    @Singleton
    public DataBaseHelper provideDatabaseHelper(Application context) {
        return new DataBaseHelper(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Provides
    @Singleton
    public AndroidDatabaseSource provideDatabaseSource(Application context, DataBaseHelper helper) {
        return new AndroidDatabaseSource(helper);
    }

    @Provides
    @Singleton
    public RequestQueue provideRequestQueue(Application context){
        return Volley.newRequestQueue(context);
    }

    @Provides
    @Singleton
    public AndroidNetworkDataSource provideNetworkDataSource(RequestQueue queue, Application context) {
        return new AndroidNetworkDataSource(queue);
    }

    @Provides
    @Singleton
    public UserRepository provideRepository(AndroidNetworkDataSource networkDataSource, AndroidDatabaseSource databaseDataSource){
        return new UserRepository(networkDataSource, databaseDataSource);
    }

    @Provides
    public UseCaseCaller provideUserCaseCaller(Application context){
        return new AndroidUseCaseCaller(context);
    }
}
