package com.tuenti.tuentitestsantiago.view.base.service;

import android.app.IntentService;

import com.tuenti.tuentitestsantiago.view.base.BaseApplication;


public abstract class BaseIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BaseIntentService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        injectDependencies();
    }

    private void injectDependencies() {
        ((BaseApplication) getApplicationContext()).inject(this);
    }

}
