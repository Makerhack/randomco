package com.tuenti.tuentitestsantiago.view.ui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tuenti.tuentitestsantiago.R;
import com.tuenti.tuentitestsantiago.domain.model.Location;
import com.tuenti.tuentitestsantiago.domain.model.Picture;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.annotations.MyPresenter;
import com.tuenti.tuentitestsantiago.view.base.fragment.PresenterFragment;
import com.tuenti.tuentitestsantiago.view.ui.presenter.UsertDetailPresenter;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Class showing a user extended information
 * Created by seven on 22/04/2017.
 */

public class UsertDetailFragment extends PresenterFragment implements UsertDetailPresenter.View {

    @Inject
    @MyPresenter
    protected UsertDetailPresenter presenter;

    @BindView(R.id.picture_iv)
    protected ImageView pictureIv;
    @BindView(R.id.name_tv)
    protected TextView nameTv;
    @BindView(R.id.other_info_tv)
    protected TextView otherInfoTv;

    public static final String TAG = UsertDetailFragment.class.getSimpleName();
    private User user;
    private String userId;

    public static UsertDetailFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString(Constants.Keys.USER_ID, userId);
        UsertDetailFragment fragment = new UsertDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            userId = arguments.getString(Constants.Keys.USER_ID);
        }
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(Constants.Keys.USER_ID)) {
                userId = savedInstanceState.getString(Constants.Keys.USER_ID);
            }
            if (savedInstanceState.containsKey(Constants.Keys.USER)) {
                user = (User) savedInstanceState.getSerializable(Constants.Keys.USER);
            }
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user_detail;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (user == null) {
            presenter.getUser(userId);
        } else {
            drawUser();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.Keys.USER_ID, userId);
        outState.putSerializable(Constants.Keys.USER, (Serializable) user);
        super.onSaveInstanceState(outState);
    }

    private void drawUser() {
        if (user == null || !isAdded()) {
            return;
        }
        nameTv.setText(user.getName().getFirst() + " " + user.getName().getLast());
        Picture picture = user.getPicture();
        if (picture != null && !TextUtils.isEmpty(picture.getThumbnail())) {
            Glide.with(getContext()).load(picture.getThumbnail()).into(pictureIv);
        }
        String info = "";
        String gender = user.getGender();
        if (!TextUtils.isEmpty(gender)) {
            info += getString(R.string.gender, gender) + "\n";
        }
        Location location = user.getLocation();
        if (location != null) {
            String locationString = location.getStreet() + " " + location.getCity() + " " + location.getState();
            if (!TextUtils.isEmpty(locationString)) {
                info += getString(R.string.location, locationString) + "\n";
            }
        }
        String registered = user.getRegistered();
        if (!TextUtils.isEmpty(registered)) {
            info += getString(R.string.registered, registered) + "\n";
        }
        String email = user.getEmail();
        if (!TextUtils.isEmpty(email)) {
            info += getString(R.string.email, email) + "\n";
        }
        otherInfoTv.setText(info);
    }

    @Override
    protected void refresh() {
        presenter.getUser(userId);
    }

    @Override
    public void onUser(User user) {
        this.user = user;
        if (isAdded()) {
            drawUser();
        }
    }

    @Override
    public void showUserError() {
        showMessage(R.string.no_user);
        if (isAdded()) {
            getActivity().finish();
        }
    }
}
