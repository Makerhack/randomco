package com.tuenti.tuentitestsantiago.view.base.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;


public abstract class BaseActivity extends AppCompatActivity {

    protected boolean homeButtonEnabled = true;


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            if (shouldSetActionBarBack()) {
                supportActionBar.setDisplayHomeAsUpEnabled(homeButtonEnabled);
                supportActionBar.setHomeButtonEnabled(homeButtonEnabled);
            }
            if (shouldShowHomeIcon()) {
                supportActionBar.setDisplayUseLogoEnabled(true);
            }
        }
    }

    protected boolean shouldSetActionBarBack() {
        return true;
    }

    protected boolean shouldShowHomeIcon() {
        return false;
    }

}
