package com.tuenti.tuentitestsantiago.view.executor;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.tuenti.tuentitestsantiago.domain.UseCaseCaller;
import com.tuenti.tuentitestsantiago.view.Constants;

import java.util.ArrayList;

/**
 * Usecase caller which uses the android framework to call usecases with an IntentService
 * Created by seven on 22/04/2017.
 */

public class AndroidUseCaseCaller implements UseCaseCaller {

    private Context context;

    public AndroidUseCaseCaller(Context context){

        this.context = context;
    }

    @Override
    public void callUseCase(int useCaseNumber, boolean forceUpdate, ArrayList<String> params) {
        Intent intent = new Intent(context, ExecutorIntentService.class);
        intent.putExtra(Constants.UseCases.USE_CASE, useCaseNumber);
        intent.putExtra(Constants.UseCases.FORCE_UPDATE, forceUpdate);
        intent.putStringArrayListExtra(Constants.UseCases.PARAMS, params);
        new Handler().postDelayed(() -> context.startService(intent), 100);
    }
}
