package com.tuenti.tuentitestsantiago.view.base.activity;

import android.os.Bundle;

import com.tuenti.tuentitestsantiago.view.base.BaseApplication;

import java.util.List;

import dagger.ObjectGraph;

public abstract class DaggerActivity extends BaseActivity {
    private ObjectGraph activityScopeGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
    }

    private void injectDependencies() {
        List<Object> modules = getModules();
        modules.add(new ActivityModule(this));
        activityScopeGraph = getInjectableApplication().plus(modules);
        inject(this);
    }

    private BaseApplication getInjectableApplication() {
        return (BaseApplication) getApplication();
    }

    public void inject(Object object) {
        activityScopeGraph.inject(object);
    }

    protected abstract List<Object> getModules();

}
