package com.tuenti.tuentitestsantiago.view.base.presenter;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.UseCaseCaller;
import com.tuenti.tuentitestsantiago.domain.model.DataError;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.ViewInterface;

import java.util.ArrayList;
import java.util.List;


/**
 * Basic class for View controllers used in distinct Fragments
 * Created by Santi on 18/05/2014.
 */
public abstract class BasePresenter<T extends BasePresenter.View>
        implements Presenter<T>, WorkerPresenter {

    protected Bus bus;
    protected T view;
    private List<Integer> works = new ArrayList<>();
    private UseCaseCaller useCaseCaller;
    private int workerIndex = 0;
    private boolean isRegistered = false;

    public BasePresenter(Bus bus, UseCaseCaller useCaseCaller) {
        this.bus = bus;
        this.useCaseCaller = useCaseCaller;
    }

    @Override
    public boolean isRegistered() {
        return isRegistered;
    }

    @Override
    public void onResume(T ui) {
        this.view = ui;
        if (!isRegistered) {
            bus.register(this);
            isRegistered = true;
        }
    }

    @Override
    public void onPause() {
        if (view != null && view.hasLoadingView() && isWorking()) {
            view.hideLoad();
        }
        stopWorking(workerIndex);
        this.view = null;
        if (isRegistered) {
            bus.unregister(this);
            isRegistered = false;
        }
    }


    protected void callUseCase(int useCaseNumber, List<String> params, boolean forceUpdate) {
        useCaseCaller.callUseCase(useCaseNumber, forceUpdate,
                params != null ? new ArrayList<>(params) : null);
        if (view != null) {
            workerIndex = startWorking();
        }
    }

    @Override
    public void callUseCase(int useCaseNumber, List<String> params) {
        callUseCase(useCaseNumber, params, true);
    }

    protected void onSuccess() {
        stopWorking(workerIndex);
    }

    protected void onError(DataError error) {
        stopWorking(workerIndex);
        if (!isRegistered()) {
            //This presenter it's not currently with a view showing, so ignore the event
            return;
        }
        switch (error.getErrorCode()) {
            case Constants.Errors.EMPTY_RESPONSE:
                view.showEmptyError();
                break;
            case Constants.Errors.ERROR_CANT_PARSE:
                view.showParseError();
                break;
            case Constants.Errors.SERVER_ERROR:
            case Constants.Errors.ERROR_SERVICE_UNAVAILABLE:
                view.showConnectionError();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean hasLoadingView() {
        return view.hasLoadingView();
    }


    @Override
    public void stopAllWorks() {
        if (view.hasLoadingView()) {
            view.hideLoad();
        }
        works.clear();
    }

    @Override
    public int startWorking() {
        works.add(++workerIndex);
        if (isRegistered() && view.hasLoadingView()) {
            view.showLoad();
        }
        return workerIndex;
    }

    @Override
    public void stopWorking(int workIndex) {
        if (isRegistered() && view.hasLoadingView()) {
            view.hideLoad();
        }
        if (isRegistered()) {
            works.remove(Integer.valueOf(workIndex));
        } else {
            works = new ArrayList<>();
        }
    }

    @Override
    public boolean isWorking() {
        return works != null && !works.isEmpty();
    }


    public interface View extends ViewInterface {

    }
}
