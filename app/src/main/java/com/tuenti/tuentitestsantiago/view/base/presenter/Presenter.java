package com.tuenti.tuentitestsantiago.view.base.presenter;

import android.os.Bundle;

import java.util.List;

/**
 * Interface defining a presenter class
 * Created by seven on 22/04/2017.
 */

public interface Presenter<T> {

    boolean isRegistered();
    void onResume(T ui);
    void onPause();
    void callUseCase(int useCaseNumber, List<String> params);
}
