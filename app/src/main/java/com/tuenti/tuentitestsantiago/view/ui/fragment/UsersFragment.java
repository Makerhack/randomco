package com.tuenti.tuentitestsantiago.view.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.tuenti.tuentitestsantiago.R;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.annotations.MyPresenter;
import com.tuenti.tuentitestsantiago.view.base.common.recyclerviewutils.DividerItemDecorator;
import com.tuenti.tuentitestsantiago.view.base.fragment.PresenterFragment;
import com.tuenti.tuentitestsantiago.view.ui.adapter.UsersAdapter;
import com.tuenti.tuentitestsantiago.view.ui.presenter.UsersPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Fragment to display an user list
 * Created by seven on 22/04/2017.
 */

public class UsersFragment extends PresenterFragment implements UsersPresenter.View {

    public static final String TAG = UsersFragment.class.getSimpleName();

    public static UsersFragment newInstance() {
        Bundle args = new Bundle();
        UsersFragment fragment = new UsersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.Actions.ACTION_DELETE_USER.equals(intent.getAction())) {
                presenter.deleteUser(intent.getStringExtra(Constants.Keys.USER_ID));
            } else if (Constants.Actions.ACTION_FAV_USER.equals(intent.getAction())) {
                presenter.favUser(intent.getStringExtra(Constants.Keys.USER_ID),
                        intent.getBooleanExtra(Constants.Keys.IS_FAV, false));
            }
        }
    };

    @Inject
    @MyPresenter
    protected UsersPresenter presenter;

    @BindView(R.id.users_rv)
    protected RecyclerView usersRv;

    private UsersAdapter adapter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        usersRv.setLayoutManager(new LinearLayoutManager(getContext()));
        usersRv.addItemDecoration(new DividerItemDecorator(getContext(),
                DividerItemDecorator.VERTICAL_LIST));
        adapter = new UsersAdapter(getContext());
        usersRv.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.Actions.ACTION_DELETE_USER);
        filter.addAction(Constants.Actions.ACTION_FAV_USER);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                broadcastReceiver, filter);
        presenter.getUsers(false);
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.ic_action_more:{
                presenter.getMoreUsers();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_users;
    }

    @Override
    protected void refresh() {
        presenter.getUsers(true);
    }

    @Override
    public void onUsers(List<User> users) {
        adapter.set(users);
    }

    @Override
    public void showDeleteError() {
        showMessage(R.string.delete_error);
    }

    @Override
    public void showDeleteOk() {
        showMessage(R.string.delete_ok);
    }

    @Override
    public void showFavError() {
        showMessage(R.string.fav_error);
    }

    @Override
    public void showFavOk() {
        showMessage(R.string.fav_ok);
    }
}
