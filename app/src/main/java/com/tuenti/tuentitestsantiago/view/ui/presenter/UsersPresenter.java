package com.tuenti.tuentitestsantiago.view.ui.presenter;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.tuenti.tuentitestsantiago.domain.UseCaseCaller;
import com.tuenti.tuentitestsantiago.domain.busresponses.DeleteUserResponse;
import com.tuenti.tuentitestsantiago.domain.busresponses.FavUserResponse;
import com.tuenti.tuentitestsantiago.domain.busresponses.UsersResponse;
import com.tuenti.tuentitestsantiago.domain.model.DataError;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.presenter.BasePresenter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Presemter for UsersFragment
 * Created by seven on 22/04/2017.
 */

public class UsersPresenter extends BasePresenter<UsersPresenter.View> {

    public UsersPresenter(Bus bus, UseCaseCaller caller) {
        super(bus, caller);
    }

    public void getUsers(boolean refresh) {
        callUseCase(Constants.UseCases.GET_USERS, null, refresh);
    }

    @Subscribe
    public void onSuccess(UsersResponse response) {
        super.onSuccess();
        view.onUsers(response.getResponse());
    }

    @Subscribe
    public void onSuccess(DeleteUserResponse response) {
        super.onSuccess();
        Boolean deleted = response.getResponse();
        if (!deleted) {
            view.showDeleteError();
        } else {
            view.showDeleteOk();
        }
    }

    @Subscribe
    public void onSuccess(FavUserResponse response) {
        super.onSuccess();
        Boolean deleted = response.getResponse();
        if (!deleted) {
            view.showFavError();
        } else {
            view.showFavOk();
        }
    }

    @Subscribe
    public void onError(DataError error) {
        super.onError(error);
    }

    public void deleteUser(String userId) {
        callUseCase(Constants.UseCases.DELETE_USER, Collections.singletonList(userId));
    }

    public void favUser(String userId, boolean fav) {
        callUseCase(Constants.UseCases.FAV_USER, Arrays.asList(userId, String.valueOf(fav)));
    }

    public void getMoreUsers() {
        callUseCase(Constants.UseCases.GET_MORE_USERS, null, true);
    }

    public interface View extends BasePresenter.View {
        void onUsers(List<User> users);

        void showDeleteError();

        void showDeleteOk();

        void showFavError();

        void showFavOk();
    }


}
