package com.tuenti.tuentitestsantiago.view.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.activity.DaggerActivity;
import com.tuenti.tuentitestsantiago.view.ui.fragment.UsertDetailFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by seven on 22/04/2017.
 */

public class UserDetailActivity extends DaggerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            String userId = getIntent().getStringExtra(Constants.Keys.USER_ID);
            if (TextUtils.isEmpty(userId)) {
                finish();
            }
            ft.replace(android.R.id.content, UsertDetailFragment.newInstance(userId),
                    UsertDetailFragment.TAG);
            ft.commit();
        }
    }

    @Override
    protected List<Object> getModules() {
        ArrayList<Object> modules = new ArrayList<>();
        modules.add(new UserDetailModule());
        return modules;
    }
}
