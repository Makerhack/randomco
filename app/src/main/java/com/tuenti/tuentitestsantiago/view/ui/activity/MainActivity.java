package com.tuenti.tuentitestsantiago.view.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

import com.tuenti.tuentitestsantiago.R;
import com.tuenti.tuentitestsantiago.view.base.activity.DaggerActivity;
import com.tuenti.tuentitestsantiago.view.ui.fragment.UsersFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends DaggerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(android.R.id.content, UsersFragment.newInstance(),
                    UsersFragment.TAG);
            ft.commit();
        }
    }

    @Override
    protected List<Object> getModules() {
        ArrayList<Object> objects = new ArrayList<>();
        objects.add(new MainActivityModule());
        return objects;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.users, menu);
        return true;
    }

    //Decorator pattern, let's define how this activity'a ActionBar should look like

    @Override
    protected boolean shouldSetActionBarBack() {
        return false;
    }

    @Override
    protected boolean shouldShowHomeIcon() {
        return true;
    }
}
