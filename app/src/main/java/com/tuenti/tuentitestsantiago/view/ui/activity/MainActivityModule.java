package com.tuenti.tuentitestsantiago.view.ui.activity;

import android.content.Context;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.UseCaseCaller;
import com.tuenti.tuentitestsantiago.view.base.annotations.ActivityContext;
import com.tuenti.tuentitestsantiago.view.base.annotations.MyPresenter;
import com.tuenti.tuentitestsantiago.view.ui.fragment.UsersFragment;
import com.tuenti.tuentitestsantiago.view.ui.presenter.UsersPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true, complete = false,
        injects = {MainActivity.class, UsersFragment.class})

public class MainActivityModule {

    @Provides
    public @MyPresenter UsersPresenter provideProductsPresenter(Bus bus, UseCaseCaller caseCaller) {
        return new UsersPresenter(bus, caseCaller);
    }
}
