package com.tuenti.tuentitestsantiago.view.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tuenti.tuentitestsantiago.R;
import com.tuenti.tuentitestsantiago.domain.model.Name;
import com.tuenti.tuentitestsantiago.domain.model.Picture;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.common.recyclerviewutils.ItemRecyclerAdapter;
import com.tuenti.tuentitestsantiago.view.base.common.recyclerviewutils.ItemViewHolder;
import com.tuenti.tuentitestsantiago.view.ui.activity.UserDetailActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by seven on 22/04/2017.
 */

public class UsersAdapter extends ItemRecyclerAdapter<User, UsersAdapter.UserHolder> {

    public UsersAdapter(Context context) {
        super(context);
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = getLayoutInflater().inflate(R.layout.row_user, viewGroup, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position, User item) {
        Name name = item.getName();
        if (name != null) {
            holder.nameTv.setText(name.getFirst() + " " + name.getLast());
        } else {
            holder.nameTv.setText("");
        }
        holder.emailTv.setText(item.getEmail());
        holder.phoneTv.setText(item.getPhone());
        Picture picture = item.getPicture();
        if (picture != null) {
            String thumbnail = picture.getThumbnail();
            if (!TextUtils.isEmpty(thumbnail)) {
                Glide.with(context).load(thumbnail).into(holder.pictureIv);
                holder.pictureIv.setOnClickListener(view -> {
                    Intent intent = new Intent(context, UserDetailActivity.class);
                    intent.putExtra(Constants.Keys.USER_ID, String.valueOf(item.getId()));
                    context.startActivity(intent);
                });
            } else {
                holder.pictureIv.setImageResource(R.drawable.logo);
            }
        } else {
            holder.pictureIv.setImageResource(R.drawable.logo);
        }
        if (item.isFavorite()) {
            holder.favoriteIb.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            holder.favoriteIb.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }
        holder.deleteIb.setOnClickListener((v) -> {
            deleteUser(String.valueOf(item.getId()));
            getItems().remove(item);
            notifyItemRemoved(holder.getAdapterPosition());
        });
        holder.favoriteIb.setOnClickListener((v) -> {
            favUser(String.valueOf(item.getId()), item.isFavorite());
            item.setFavorite(!item.isFavorite());
            notifyItemChanged(holder.getAdapterPosition());
        });
    }

    private void deleteUser(String userId) {
        Intent intent = new Intent(Constants.Actions.ACTION_DELETE_USER);
        intent.putExtra(Constants.Keys.USER_ID, userId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

    private void favUser(String userId, boolean fav) {
        Intent intent = new Intent(Constants.Actions.ACTION_FAV_USER);
        intent.putExtra(Constants.Keys.USER_ID, userId);
        intent.putExtra(Constants.Keys.IS_FAV, !fav);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    static class UserHolder extends ItemViewHolder<User> {

        @BindView(R.id.name_tv)
        TextView nameTv;
        @BindView(R.id.phone_tv)
        TextView phoneTv;
        @BindView(R.id.email_tv)
        TextView emailTv;
        @BindView(R.id.picture_iv)
        ImageView pictureIv;
        @BindView(R.id.favorite_ib)
        ImageButton favoriteIb;
        @BindView(R.id.delete_ib)
        ImageButton deleteIb;

        UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
