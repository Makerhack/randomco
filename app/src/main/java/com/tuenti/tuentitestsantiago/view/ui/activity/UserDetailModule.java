package com.tuenti.tuentitestsantiago.view.ui.activity;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.UseCaseCaller;
import com.tuenti.tuentitestsantiago.view.base.annotations.MyPresenter;
import com.tuenti.tuentitestsantiago.view.ui.fragment.UsersFragment;
import com.tuenti.tuentitestsantiago.view.ui.fragment.UsertDetailFragment;
import com.tuenti.tuentitestsantiago.view.ui.presenter.UsersPresenter;
import com.tuenti.tuentitestsantiago.view.ui.presenter.UsertDetailPresenter;

import dagger.Module;
import dagger.Provides;

@Module(library = true, complete = false,
        injects = {UserDetailActivity.class, UsertDetailFragment.class})

public class UserDetailModule {

    @Provides
    public @MyPresenter
    UsertDetailPresenter provideProductsPresenter(Bus bus, UseCaseCaller caseCaller) {
        return new UsertDetailPresenter(bus, caseCaller);
    }
}
