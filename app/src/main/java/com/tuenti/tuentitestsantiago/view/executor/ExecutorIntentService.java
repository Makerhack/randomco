package com.tuenti.tuentitestsantiago.view.executor;

import android.content.Intent;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.Executor;
import com.tuenti.tuentitestsantiago.domain.UseCase;
import com.tuenti.tuentitestsantiago.domain.usecases.DeleteUserUseCase;
import com.tuenti.tuentitestsantiago.domain.usecases.FavUserUseCase;
import com.tuenti.tuentitestsantiago.domain.usecases.GetMoreUsersUseCase;
import com.tuenti.tuentitestsantiago.domain.usecases.GetUserUseCase;
import com.tuenti.tuentitestsantiago.domain.usecases.GetUsersUseCase;
import com.tuenti.tuentitestsantiago.repository.UserRepository;
import com.tuenti.tuentitestsantiago.view.Constants;
import com.tuenti.tuentitestsantiago.view.base.service.BaseIntentService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class ExecutorIntentService extends BaseIntentService implements Executor {

    @Inject
    protected Bus bus;
    @Inject
    protected UserRepository repository;

    public ExecutorIntentService() {
        super("ExecutorIntentService");
    }

    @Override
    public void execute(UseCase useCase, List<String> params) {
        useCase.setParams(params);
        useCase.execute();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            ArrayList<String> params = intent.getStringArrayListExtra(Constants.UseCases.PARAMS);
            int useCaseNumber = intent.getIntExtra(Constants.UseCases.USE_CASE, 0);
            UseCase useCase = getUseCase(useCaseNumber);
            useCase.setForceUpdate(intent.getBooleanExtra(Constants.UseCases.FORCE_UPDATE, false));
            execute(useCase, params);
        }
    }

    public UseCase getUseCase(int caseNumber) {
        switch (caseNumber) {
            case Constants.UseCases.GET_USERS:
                return new GetUsersUseCase(bus, repository);
            case Constants.UseCases.DELETE_USER:
                return new DeleteUserUseCase(bus, repository);
            case Constants.UseCases.FAV_USER:
                return new FavUserUseCase(bus, repository);
            case Constants.UseCases.GET_MORE_USERS:
                return new GetMoreUsersUseCase(bus, repository);
            case Constants.UseCases.GET_USER:
                return new GetUserUseCase(bus, repository);
        }
        return null;
    }

}
