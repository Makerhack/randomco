package com.tuenti.tuentitestsantiago.domain.usecases;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.BaseUseCase;
import com.tuenti.tuentitestsantiago.domain.busresponses.FavUserResponse;
import com.tuenti.tuentitestsantiago.domain.busresponses.UserDetailResponse;
import com.tuenti.tuentitestsantiago.domain.model.User;
import com.tuenti.tuentitestsantiago.repository.UserRepository;

import java.util.List;

public class FavUserUseCase extends BaseUseCase<FavUserResponse> {

    private UserRepository repository;

    public FavUserUseCase(Bus bus, UserRepository repository) {
        super(bus);
        this.repository = repository;
    }

    @Override
    public void execute() {
        FavUserResponse favResponse = new FavUserResponse();
        List<String> params = getParams();
        String userId = params.get(0);
        boolean fav = Boolean.parseBoolean(params.get(1));
        UserDetailResponse userResponse = repository.getByKey(userId);
        if (userResponse.hasError()) {
            favResponse.setResponse(false);
        } else {
            User user = userResponse.getResponse();
            user.setFavorite(fav);
            boolean save = repository.save(user);
            favResponse.setResponse(save);
        }
        handleResponse(favResponse);
    }
}
