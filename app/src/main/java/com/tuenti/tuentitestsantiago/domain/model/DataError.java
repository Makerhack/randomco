package com.tuenti.tuentitestsantiago.domain.model;

import android.os.Parcel;

import java.io.Serializable;

/**
 * Class representing an error
 * Created by Santi on 10/04/2015.
 */
public class DataError implements Serializable {
    public static final int NO_ERROR = 0;
    protected String errorMessage;
    protected int errorCode;

    public DataError() {
        this.errorCode = 0;
        this.errorMessage = "";
    }

    public DataError(int errorCode) {
        this.errorCode = errorCode;
    }

    public DataError(int errorCode, String message) {
        this(errorCode);
        this.errorMessage = message;
    }

    public DataError(Parcel in) {
        errorMessage = in.readString();
        errorCode = in.readInt();
    }

    public boolean hasError() {
        return errorCode != 0;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

}
