package com.tuenti.tuentitestsantiago.domain;

import com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse;

import java.util.List;


public interface UseCase<T extends DataResponse> {

    void execute();

    void postErrorToBus(T response);

    boolean hasError(T response);

    void postResponseToBus(T response);

    void setParams(List<String> params);

    void setForceUpdate(boolean booleanExtra);
}
