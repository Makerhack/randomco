package com.tuenti.tuentitestsantiago.domain.usecases;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.BaseUseCase;
import com.tuenti.tuentitestsantiago.domain.busresponses.DeleteUserResponse;
import com.tuenti.tuentitestsantiago.repository.UserRepository;

import java.util.List;

public class DeleteUserUseCase extends BaseUseCase<DeleteUserResponse> {

    private UserRepository repository;

    public DeleteUserUseCase(Bus bus, UserRepository repository) {
        super(bus);
        this.repository = repository;
    }

    @Override
    public void execute() {
        List<String> params = getParams();
        boolean deleted = repository.deleteByKey(params.get(0));
        handleResponse(new DeleteUserResponse(deleted));
    }
}
