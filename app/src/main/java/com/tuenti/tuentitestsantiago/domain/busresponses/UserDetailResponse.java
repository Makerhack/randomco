package com.tuenti.tuentitestsantiago.domain.busresponses;

import com.tuenti.tuentitestsantiago.domain.model.User;

public class UserDetailResponse extends DataResponse<User> {

    public UserDetailResponse(){
        super();
    }

    public UserDetailResponse(User user){
        super(user);
    }

    public UserDetailResponse(DataResponse<User> userDataResponse){
        super(userDataResponse);
    }
}
