package com.tuenti.tuentitestsantiago.domain.usecases;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.BaseUseCase;
import com.tuenti.tuentitestsantiago.domain.busresponses.UserDetailResponse;
import com.tuenti.tuentitestsantiago.domain.busresponses.UsersResponse;
import com.tuenti.tuentitestsantiago.repository.UserRepository;

import java.util.List;

public class GetUserUseCase extends BaseUseCase<UserDetailResponse> {

    private UserRepository repository;

    public GetUserUseCase(Bus bus, UserRepository repository) {
        super(bus);
        this.repository = repository;
    }

    @Override
    public void execute() {
        List<String> params = getParams();
        String userId = params.get(0);
        handleResponse(repository.getByKey(userId));
    }
}
