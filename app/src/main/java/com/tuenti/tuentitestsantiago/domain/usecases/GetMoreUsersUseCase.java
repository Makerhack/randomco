package com.tuenti.tuentitestsantiago.domain.usecases;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.BaseUseCase;
import com.tuenti.tuentitestsantiago.domain.busresponses.UsersResponse;
import com.tuenti.tuentitestsantiago.repository.UserRepository;

public class GetMoreUsersUseCase extends BaseUseCase<UsersResponse> {

    private UserRepository repository;

    public GetMoreUsersUseCase(Bus bus, UserRepository repository) {
        super(bus);
        this.repository = repository;
    }

    @Override
    public void execute() {
        handleResponse(repository.getMore());
    }
}
