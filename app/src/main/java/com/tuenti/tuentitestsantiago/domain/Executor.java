package com.tuenti.tuentitestsantiago.domain;

import android.os.Bundle;

import com.tuenti.tuentitestsantiago.domain.UseCase;

import java.util.List;

public interface Executor {

    void execute(UseCase useCase, List<String> params);

}
