package com.tuenti.tuentitestsantiago.domain;

import com.squareup.otto.Bus;
import com.tuenti.tuentitestsantiago.domain.model.DataError;
import com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse;

import java.util.List;

public abstract class BaseUseCase<T extends DataResponse> implements UseCase<T> {
    protected Bus bus;
    protected List<String> params;
    protected boolean forceUpdate = false;

    public BaseUseCase(Bus bus) {
        this.bus = bus;
    }

    /**
     * Get the params for this UseCase
     *
     * @return the params of this use case as a Bundle object, or null if no params are set
     */
    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    /**
     * Post this DataResponse's response or error to the EventBus. Do not override this method, for more logic you should override
     * {@link #postResponseToBus(com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse)} or {@link #postErrorToBus(com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse)}
     *
     * @param response the response to be handled
     */
    protected final void handleResponse(T response) {
        if (hasError(response)) {
            postErrorToBus(response);
        } else {
            postResponseToBus(response);
        }
    }

    /**
     * Posts the DataResponse's error to the EventBus
     * Should override this method if you want to do more logic apart from posting the error
     *
     * @param response whose error will be posted to the EventBus
     */
    public void postErrorToBus(T response) {
        bus.post(new DataError(response.getError()));
    }

    /**
     * Checks if this DataResponse has a network or internal error
     *
     * @return true if {@link com.tuenti.tuentitestsantiago.domain.busresponses.DataResponse#getError()} is distinct than 0, false otherwise
     */
    public boolean hasError(T response) {
        int error = response.getError();
        return error != 0;
    }

    /**
     * Posts the DataResponse's response to the EventBus
     * Should override this method if you want to do more logic apart from posting the response
     *
     * @param response to be posted to the event bus
     */
    public void postResponseToBus(T response) {
        bus.post(response);
    }


    @Override
    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }
}
