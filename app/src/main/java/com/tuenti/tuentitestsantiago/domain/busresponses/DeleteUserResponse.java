package com.tuenti.tuentitestsantiago.domain.busresponses;

import com.tuenti.tuentitestsantiago.domain.model.User;

public class DeleteUserResponse extends DataResponse<Boolean> {

    public DeleteUserResponse(){
        super();
    }

    public DeleteUserResponse(Boolean user){
        super(user);
    }

    public DeleteUserResponse(DataResponse<Boolean> userDataResponse){
        super(userDataResponse);
    }
}
